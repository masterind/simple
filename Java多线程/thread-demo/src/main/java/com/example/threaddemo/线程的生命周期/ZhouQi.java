package com.example.threaddemo.线程的生命周期;


/**
 * @Description: 线程的生命周期
 * @Param:
 * @return:
 * @Author: mastermind
 * @Date: 2020-09-16 10:22
 */
public class ZhouQi {

    public static void main(String[] args) {

        //1、使用new关键字或其他方式创建 Thread 对象。线程处【新建】
        //2、当调用了 .start()方法，线程处于 【就绪】 。
        //3、当前线程在就绪状态，或得了CPU调度，线程处于【运行】。
        //4、当执行完 run(),线程处于 【死亡】。

        //说明在【就绪】、【等待】的线程都有可能，导致 【死亡】。
        //处于 【阻塞】会重新进入 【就绪】

    }
}
