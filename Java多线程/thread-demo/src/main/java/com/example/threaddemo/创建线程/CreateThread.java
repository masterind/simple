package com.example.threaddemo.创建线程;

/**
 * @Description: 创建线程的第一种方式
 * @Param:
 * @return:
 * @Author: mastermind
 * @Date: 2020-09-16 10:10
 */
public class CreateThread {

    public static void main(String[] args) throws Exception {

        Thread thread = new Thread("mastermind-one") {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.err.println("qwqwq");
            }
        };
        thread.start();

        System.err.println("asasasaas");

    }
}
